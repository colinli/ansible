#!/bin/sh
set -e

case "$VCS" in
    git)
        if [ -x .git/hooks/post-commit ]; then
            if ! grep -q "etckeeper post-commit" .git/hooks/post-commit; then
                echo "etckeeper warning: .git/hooks/post-commit needs to be manually modified to run: etckeeper post-commit -d `pwd`" >&2
            fi
        else
            cat >.git/hooks/post-commit <<EOF
#!/bin/sh
# post-commit hook for etckeeper, to send email notifications
set -e
etckeeper post-commit -d `pwd`
EOF
        chmod +x .git/hooks/post-commit
        fi
    ;;
    hg)
        if [ -e .hg/hgrc ] && grep "^\[hooks\]" .hg/hgrc; then
            if ! grep "^post-commit" .hg/hgrc | grep -q "etckeeper post-commit"; then
                echo "etckeeper warning: [hooks] section in .hg/hgrc needs to be manually modified to contain: post-commit = etckeeper post-commit -d `pwd`" >&2
            fi
        else
            touch .hg/hgrc
            cat >>.hg/hgrc <<EOF
[hooks]
# post-commit hook for etckeeper, to send email notifications
post-commit = etckeeper post-commit -d `pwd`
EOF
        fi
    ;;
    darcs)
        if [ -e _darcs/postfs/defaults ]; then
            if ! ( grep -q "record posthook etckeeper post-commit" _darcs/postfs/defaults &&
                grep -q "whatsnew posthook etckeeper post-commit" _darcs/postfs/defaults ); then
                echo "etckeeper warning: _darcs/postfs/defaults needs to be manually modified to run: etckeeper post-commit -d `pwd`" >&2
            fi
        else
            cat >_darcs/postfs/defaults <<EOF
record posthook etckeeper post-commit -d `pwd`
record run-posthook
whatsnew posthook etckeeper post-commit -d `pwd`
whatsnew run-posthook
EOF
        fi
    ;;
esac
