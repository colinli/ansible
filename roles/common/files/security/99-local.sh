# Set secure system-wide umask
umask 0077

# Set TMOUT to terminate inactive sessions
export TMOUT=36000
