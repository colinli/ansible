---
- name: Install ufw
  apt:
    name: ufw
  tags: firewall

- name: Dot not log UFW messages to kern.log (too verbose), ufw.log is enough
  replace:
    name: /etc/rsyslog.d/20-ufw.conf
    regexp: "^#& (stop|~)"
    replace: '& \1'
  notify: Restart rsyslog
  tags: firewall

- name: Disable IPv6 in ufw for Docker containers
  lineinfile:
    name: /etc/default/ufw
    regex: "^IPV6="
    line: "IPV6=no"
  when: ansible_virtualization_type == 'docker'

- name: Ensure at least SSH is open
  ufw:
    name: OpenSSH
    rule: limit
  when: ansible_virtualization_type != 'docker'
  tags:
    - firewall

- name: Set default input policy
  ufw:
    direction: incoming
    policy: deny
  tags: firewall

- name: Set default output policy
  ufw:
    direction: outgoing
    policy: reject
  tags: firewall

- name: Allow outgoing DNS requests to dns_resolvers
  ufw:
    name: DNS
    rule: allow
    direction: out
    dest: "{{ item }}"
  with_items: "{{ dns_resolvers }}"
  tags:
    - firewall

- name: Allow outgoing NTP requests to ntp_server_ips
  ufw:
    rule: allow
    direction: out
    dest: "{{ item }}"
    port: 123
  with_items: "{{ ntp_server_ips }}"
  tags:
    - firewall

- name: Allow outgoing HTTP(S) requests to fw_allow_http
  ufw:
    name: WWW Full
    direction: out
    rule: allow
    dest: "{{ item }}"
  with_items: "{{ fw_allow_http }}"
  tags:
    - firewall

- name: Allow outgoing SMTP requests
  ufw:
    name: SMTP
    direction: out
    rule: allow
  when: hosted_by is not defined
  tags:
    - firewall

- name: Allow outgoing SMTP requests to SMTP relayhost
  ufw:
    name: SMTP
    direction: out
    rule: allow
    to: "{{ hostvars[hosted_by].ansible_all_ipv4_addresses |ipaddr('10.0.0.0/24') |first }}"
  when: hosted_by is defined
  tags:
    - firewall

- name: Enable ufw
  ufw:
    state: enabled
  tags:
    - firewall

- name: Allow outgoing HTTP trafic to APT proxy
  ufw:
    rule: allow
    dest: 10.0.0.254
    proto: tcp
    port: 3142
    direction: out
  when: use_apt_proxy
  tags:
    - firewall

- name: Allow outgoing DHCP requests
  ufw:
    rule: allow
    from_port: 68
    port: 67
    direction: out
  when: ansible_virtualization_role == 'guest'
  tags:
    - firewall

- name: Allow outgoing ICMP
  lineinfile:
    name: /etc/ufw/before.rules
    insertbefore: "^# don't delete the 'COMMIT' line"
    line: "-A ufw-before-output -p icmp -j ACCEPT"
  notify: Notify user to restart ufw
  tags: firewall

- name: Allow outgoing ICMP6
  lineinfile:
    name: /etc/ufw/before6.rules
    insertbefore: "^# don't delete the 'COMMIT' line"
    line: "-A ufw6-before-output -p icmpv6 -j ACCEPT"
  notify: Notify user to restart ufw
  tags: firewall

- name: Allow extra outgoing trafic
  ufw:
    rule: allow
    direction: out
    dest: "{{ item.dest |default('0.0.0.0') }}"
    proto: "{{ item.proto |default('tcp') }}"
    port: "{{ item.port }}"
  with_items: "{{ fw_allow_out }}"
  tags:
    - firewall
