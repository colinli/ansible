---
- name: Install required packages
  apt:
    name:
    - backupninja
    - rdiff-backup
    - cstream
    - debconf-utils
    - hwinfo
  tags: backup-client

- name: Backup server is an independant host
  set_fact:
    backup_server_ipaddr: "{{hostvars[backup_server].ansible_default_ipv4.address}}"
  when: hosted_by is not defined or backup_server != hosted_by
  tags:
    - backup-client
    - firewall

- name: Backup server is the hypervisor
  set_fact:
    backup_server_ipaddr: "{{hostvars[backup_server].ansible_all_ipv4_addresses |ipaddr('10.0.0.0/24') |first}}"
  when: hosted_by is defined and backup_server == hosted_by
  tags:
    - backup-client
    - firewall

- name: Add backup host fingerprint to known_hosts
  known_hosts:
    name: "{{backup_server_ipaddr}}"
    key: "{{backup_server_ipaddr}} ssh-ed25519 {{hostvars[backup_server].ansible_ssh_host_key_ed25519_public}}"
  tags: backup-client

- name: Add main configuration file for backupninja
  template:
    src: backupninja.conf
    dest: /etc/
  tags: backup-client

- name: Add configuration files for backupninja
  template:
    src: "{{item}}"
    dest: /etc/backup.d/
    mode: "0600"
  with_items:
    - 10-sysinfo.sys
    - 20-process.sh
    - 99-filesystem.rdiff
  tags: backup-client

- name: Add MySQL configuration files for backupninja
  template:
    src: 40-databases.mysql
    dest: /etc/backup.d/
    mode: "0600"
  when: "'databases' in group_names and db_type == 'mariadb'"
  tags: backup-client

- name: Add PostgreSQL configuration files for backupninja
  template:
    src: 40-databases.pgsql
    dest: /etc/backup.d/
    mode: "0600"
  when: "'databases' in group_names and db_type == 'postgresql'"
  tags: backup-client

- name: Allow outgoing SSH trafic to backup server
  ufw:
    name: OpenSSH
    rule: allow
    direction: out
    dest: "{{backup_server_ipaddr}}"
  tags:
    - backup-client
    - firewall
